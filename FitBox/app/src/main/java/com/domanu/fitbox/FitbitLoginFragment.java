package com.domanu.fitbox;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.view.View.X;

public class FitbitLoginFragment extends Fragment {
    private final String authenticationUrl = "https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=2284YF&redirect_uri=https://bitbucket.org/Manueeelllaaa/fitbox&scope=activity%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight&expires_in=604800";
    private final String tokenUrl = "https://api.fitbit.com/oauth2/token";
    private WebView fitbitLoginWebview;

    public FitbitLoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_fitbit_login, container, false);

        fitbitLoginWebview = (WebView) rootView.findViewById(R.id.fitbit_login_webview);
        fitbitLoginWebview.getSettings().setJavaScriptEnabled(true);
        fitbitLoginWebview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                try {
                    Log.d("Redirect URL", url);
                    if (url.contains("https://bitbucket.org/Manueeelllaaa/fitbox")) {
                        String code = url.split("=")[1].split("#")[0];
                        Log.d("Fitbit Code", code);
                        loginToFitbit(code);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });
        fitbitLoginWebview.loadUrl(authenticationUrl);

        return rootView;
    }

    private void loginToFitbit(String code) {
        try {
            OkHttpClient client = new OkHttpClient();

            RequestBody body = new FormBody.Builder()
                    .add("clientId", "2284YF")
                    .add("grant_type", "authorization_code")
                    .add("redirect_uri", "https://bitbucket.org/Manueeelllaaa/fitbox")
                    .add("code", code)
                    .build();

            Request request = new Request.Builder()
                    .url(tokenUrl)
                    .addHeader("Authorization", "Basic MjI4NFlGOmMxZmRmMDUzNTNlYzI4NThlMDc2YTY4YzRhYzc1Y2I2")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .post(body)
                    .build();
            Call call = client.newCall(request);
            Callback callback = new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String responseStr = response.body().string();
                        Log.d("Fitbit auth successful", responseStr);

                        String authorizationToken = responseStr.split(",")[0].split("\"")[3];
                        String refreshToken = responseStr.split(",")[2].split("\"")[3];

                        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(getString(R.string.authorization_token), authorizationToken);
                        editor.putString(getString(R.string.refresh_token), refreshToken);
                        editor.commit();

                        Intent intent = new Intent(getActivity(), GoalsActivity.class);
                        startActivity(intent);
                    } else {
                        Log.d("Fitbit auth failed", response.body().string());
                    }
                }
            };
            call.enqueue(callback);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
