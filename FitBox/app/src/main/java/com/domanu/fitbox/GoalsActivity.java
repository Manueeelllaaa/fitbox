package com.domanu.fitbox;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class GoalsActivity extends AppCompatActivity {
    private Button setGoalButton;
    private TextView stepsTextView;

    public GoalsActivity() {}

    private void setText(final TextView text,final String value){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setText(value);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goals);

        setGoalButton = (Button) findViewById(R.id.set_goal_button);
        setGoalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO: open dialog to edit / set goal
            }
        });

        stepsTextView = (TextView) findViewById(R.id.steps_textview);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String authorizationToken = sharedPref.getString(getString(R.string.authorization_token), "");
        Log.d("Fitbit Auth Token", authorizationToken);

        //Test Request to Fitbit
        try{
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url("https://api.fitbit.com/1/user/-/activities/date/today.json")
                    .addHeader("Authorization", "Bearer " + authorizationToken)
                    .build();
            Call call = client.newCall(request);
            Callback callback = new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String responseStr = response.body().string();
                        Log.d("Fitbit steps response", responseStr);
                        Pattern pattern = Pattern.compile("\"steps\":([0-9]*?),");
                        Matcher matcher = pattern.matcher(responseStr);
                        if (matcher.find()) {
                            Log.d("Fitbit steps", matcher.group(1));
                            setText(stepsTextView, matcher.group(1));
                        }
                    } else {
                        Log.d("Fitbit steps failed", response.body().string());
                    }
                }
            };
            call.enqueue(callback);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}